ISO profiles for TeArch Linux. Use teaiso for build profiles.

# Usage

```shell
# go teaiso profile directory
cd /usr/lib/teaiso/ 
# clone profile
git clone https://gitlab.com/tearch-linux/configs/tearch-iso-profiles
# create new build dir and go
mkdir /var/tearch && cd /var/tearch
# install tearch mirror list
curl https://gitlab.com/tearch-linux/configs/tearch-mirrorlist/-/raw/master/tearch-mirrorlist > /etc/pacman.d/tearch-mirrorlist
# run teaiso. 
mkteaiso -vp tearch-iso-profiles/editions/<desktop-name> -w work -o output
```
