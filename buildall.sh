#!/bin/bash
set -ex
for edition in xfce mate gnome cinnamon server pantheon kde ; do
    mkdir -p build/$edition
    cd build/$edition
    mkteaiso -p ../../editions/$edition -o ../../build-iso/ -w . --nocheck
    cd ../../
done
