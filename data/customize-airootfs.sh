#!/bin/bash
set -e -u

# Set default editor
export _EDITOR=nano
echo "EDITOR=${_EDITOR}" >> /etc/environment
echo "EDITOR=${_EDITOR}" >> /etc/profile

# Export locales
sed -i 's/^#\(en_US\.UTF-8\)/\1/' /etc/locale.gen; locale-gen

# Set root shell and password
usermod -p $(openssl passwd "") root
sed -i "s|^root:x:.*|root:x:0:0:root:/root:/bin/fish|g" /etc/passwd

# Set timezone
ln -sf /usr/share/zoneinfo/UTC /etc/localtime

# Add Necessary Groups
groupadd -rf autologin
groupadd -rf nopasswdlogin
groupadd -rf wheel
groupadd -rf sys
groupadd -rf rfkill

# Enable systemd services
systemctl enable 17g || true
systemctl enable lightdm  || true
systemctl enable NetworkManager systemd-resolved  || true
systemctl set-default graphical.target  || true
systemctl enable --user pipewire.service pipewire-media-session.service pipewire-pulse.service  || true

# Init Pacman Keys
pacman-key --init
pacman-key --populate archlinux tearch
pacman-key --lsign-key 0AA4D45CBAA86F73

# Misc
gtk_theme="Flat-Remix-GTK-Blue-Dark-Solid"
icon_theme="TeArch"
if [[ -d /usr/share/gnome-shell/ ]]; then 
    user_session="gnome"
elif [[ -d /usr/lib/wingpanel/ ]]; then
    user_session="pantheon-non-gnome"
    icon_theme="TeArch-Pantheon"
elif [[ -d /usr/lib/cinnamon/ ]]; then
    user_session="cinnamon"
else
    user_session="xfce"
fi
if [[ -d /etc/lightdm/ ]]; then 

# LightDM GTK Greeter Theming
cat>/etc/lightdm/lightdm-gtk-greeter.conf<< EOL
[greeter]
theme-name = ${gtk_theme}
icon-theme-name = ${icon_theme}
background = /usr/share/backgrounds/tearch/TeArch-Linux-Other.svg
position = 19%,center 40%,center
user-background = false
EOL
fi